Documentação Completa: http://localhost:9999/swagger-ui.html ou JavaDoc


Tecnologias Utilizadas:
-Maven
-SpringBoot
-Postman
-JPA
-JAVA 8
-Swagger
-PostgreSQL

---

- É necessário criar no banco de dados o DataBase chamado "product", as tabelas serão carregadas ao executar o projeto.

- A Classe DataConfiguration armazena as informações para comunicação com o Banco de Dados Postgresql, insira o username e password correspondentes ao seu banco de dados.

- Foi desenvolvido alguns testes Unitários, estão presentes na Classe "DesafioJavaSpringbootApplicationTests".