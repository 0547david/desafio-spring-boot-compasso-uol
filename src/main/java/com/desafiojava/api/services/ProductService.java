package com.desafiojava.api.services;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import com.desafiojava.api.models.Product;
import com.desafiojava.api.repository.ProductRepository;

@Service
public class ProductService {

	@Autowired
	private ProductRepository productRepository;

	/**
	 * Método Responsponsável por Salvar o Produto
	 * 
	 * @param product
	 * @return
	 */
	public ResponseEntity<?> save(Product product) {

		if (!validaProduto(product)) {
			return returnMessage(HttpStatus.BAD_REQUEST.value());
		}

		Product productCreated = productRepository.save(product);

		return new ResponseEntity<>(productCreated, HttpStatus.CREATED);
	}

	/**
	 * Metodo Responsável por atualizar o produto
	 * 
	 * @param id
	 * @param product
	 * @return
	 */
	public ResponseEntity<?> update(String id, Product product) {
		Optional<Product> pUpdate = productRepository.findById(id);

		if (Objects.isNull(pUpdate) || pUpdate.isEmpty()) {
			return returnMessage(HttpStatus.NOT_FOUND.value());
		} else {
			product.setId(id);
		}

		if (!validaProduto(product)) {
			return returnMessage(HttpStatus.BAD_REQUEST.value());
		}

		Product productUpdated = productRepository.save(product);

		return new ResponseEntity<>(productUpdated, HttpStatus.OK);
	}

	/**
	 * Metodo responsável por ealizar busca de produto por ID
	 * 
	 * @param id
	 * @return
	 * @throws IOException
	 */
	public ResponseEntity<?> findById(String id) throws IOException {
		Optional<Product> pGet = productRepository.findById(id);

		if (Objects.isNull(pGet) || pGet.isEmpty()) {
			return returnMessage(HttpStatus.NOT_FOUND.value());
		}

		return new ResponseEntity<>(pGet, HttpStatus.OK);
	}

	/**
	 * Metodo responsável por buscar todos os Produtos Cadastrados
	 * 
	 * @return
	 */
	public List<Product> findAll() {
		return productRepository.findAll();
	}

	public ResponseEntity<?> delete(String id) {
		Optional<Product> pDelete = productRepository.findById(id);

		if (Objects.isNull(pDelete) || pDelete.isEmpty()) {
			return returnMessage(HttpStatus.NOT_FOUND.value());
		}

		productRepository.deleteById(id);

		return new ResponseEntity<>(null, HttpStatus.OK);
	}

	/**
	 * Método responsável por realizar a busca de produtos utilizando filters
	 * 
	 * @param min_price
	 * @param max_price
	 * @param q
	 * @return
	 */
	public List<Product> findByFilters(float min_price, float max_price, String q) {

		min_price = !Objects.isNull(min_price) ? min_price : 0;
		max_price = !Objects.isNull(max_price) ? max_price : 999999999;

		if (max_price < min_price) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
					"O preço máximo não pode ser maio que o preço mínimo!");
		}

		if (Objects.isNull(q) || q.isEmpty()) {
			return productRepository.findByPrice(min_price, max_price);
		} else {
			return productRepository.findByPriceAndQ(min_price, max_price, q);
		}
	}

	/**
	 * Método responsável por verificar e validar os dados de cadastro de produto
	 * 
	 * @param product
	 * @return
	 */
	public Boolean validaProduto(Product product) {
		if (product.getName().isEmpty()) {
			return false;
		} else if (product.getDescription().isEmpty()) {
			return false;
		} else if (product.getPrice() < 0) {
			return false;
		}

		return true;
	}

	/**
	 * Função responsavel por preparar os responses em caso de erro
	 * @param cod
	 * @return
	 */
	public ResponseEntity<?> returnMessage(int cod) {

		if (cod == 404) {
			Map<String, String> headers = new HashMap<>();
			headers.put("message", "Produto não encontrado!");
			headers.put("status_code", "" + cod);
			return new ResponseEntity<>(headers, HttpStatus.NOT_FOUND);
		} else if (cod == 400) {
			Map<String, String> headers = new HashMap<>();
			headers.put("message", "Falha ao realizar a operação, verifique os dados e tente novamente!");
			headers.put("status_code", "" + cod);
			return new ResponseEntity<>(headers, HttpStatus.BAD_REQUEST);
		} else {
			return null;
		}
	}

}
