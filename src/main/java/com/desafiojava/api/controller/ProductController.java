package com.desafiojava.api.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.desafiojava.api.models.Product;
import com.desafiojava.api.services.ProductService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(value="Products API")
@RestController
@RequestMapping(value="/products")
public class ProductController {
	
	@Autowired 
	private ProductService productService;
	
	
	/**
	 * Metodp responsavel pela criação do produto
	 * @param product - objeto contendo informações do produto a ser criado
	 * @return - Produto criado
	 */
	@PostMapping
	@ApiOperation(value="Metodo responsável pela criação do prproduto")
	public ResponseEntity<?> createProduct(@RequestBody Product product) {
		return productService.save(product);
	}
	
	/**
	 * Realiza a alteração do produto recebido como parâmetro, caso exista 
	 * @param id - Id do produto a ser atualizado
	 * @param product - Informações a serem atualizadas
	 * @return - Produto alterado
	 */
	@PutMapping("/{id}")
	@ApiOperation(value="Realiza a alteração do produto recebido como parâmetro, caso exista")
	public ResponseEntity<?> alterProduct(@PathVariable(value="id") String id, @RequestBody Product product) {
		return productService.update(id, product);
	}
	
	/**
	 * Realiza a busca de um produto por seu Id
	 * @param id - Chave do Produto
	 * @return - Produto Buscado
	 * @throws IOException 
	 */
	@GetMapping("/{id}")
	@ApiOperation(value="Realiza a busca de um produto por seu Id")
	public ResponseEntity<?> getProduct(@PathVariable(value="id") String id) throws IOException {
		return productService.findById(id);
	}
	
	/**
	 * Metodo responsavel por realizar a busca por todos os produtos cadastrados
	 * @return -  Todos os Produtos Cadastrados
	 */
	@GetMapping
	@ApiOperation(value="Metodo responsavel por realizar a busca por todos os produtos cadastrados")
	public List<Product> getProducts() {
		return productService.findAll();
	}
	
	/**
	 * Metodo responsavel por buscar os produtos pelo price e pelos campos name e description
	 * 
	 * @param min_price - verificará se o valor é ">=" contra o campo price
	 * @param max_price - verificará se o valor é "<=" contra o campo price
	 * @param q - parametro que verificará os campos name e description
	 * @return - Lista de produtos buscados
	 */
	@GetMapping("/search")
	@ResponseBody
	@ApiOperation(value="Metodo responsavel por buscar os produtos pelo price e pelos campos name e description representados por min_price, max_price e q respectivamente.")
	public List<Product> getByFilters(@RequestParam(required=false) float min_price, @RequestParam(required=false) float max_price, @RequestParam(required=false) String q) {
		return productService.findByFilters(min_price , max_price, q);
	}
	
	/**
	 * Metodo responsável por deletar o produto recebido cujo Id foi recebido como parâmetro, caso exista!
	 * @param id - id do produto
	 * @return
	 */
	@DeleteMapping("/{id}")
	@ApiOperation(value="Metodo responsável por deletar o produto recebido cujo Id foi recebido como parâmetro, caso exista!")
	public ResponseEntity<?> deleteProduct(@PathVariable(value="id") String id) {
		return productService.delete(id);
	}
}