package com.desafiojava.api.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.desafiojava.api.models.Product;

@Repository
public interface ProductRepository extends JpaRepository<Product, String> {

	Optional<Product> findById(String id);

	@Query(value = "FROM Product WHERE price between :min_price and :max_price and (name like %:q% or description like %:q%)")
	List<Product> findByPriceAndQ(@Param("min_price") float min_price, @Param("max_price") float max_price,
			@Param("q") String q);

	@Query(value = "FROM Product WHERE price between :min_price and :max_price")
	List<Product> findByPrice(@Param("min_price") float min_price, @Param("max_price") float max_price);
}