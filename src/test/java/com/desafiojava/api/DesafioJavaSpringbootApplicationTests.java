package com.desafiojava.api;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.core.annotation.Order;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.desafiojava.api.models.Product;
import com.desafiojava.api.repository.ProductRepository;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc 
class DesafioJavaSpringbootApplicationTests {

	@Autowired
	MockMvc mockMvc;

	@Autowired
	ObjectMapper mapper;

	@MockBean
	ProductRepository productRepository;
	
	@Autowired
	private WebApplicationContext webApplicationContext;
	
	
	@Before
	public void setup() {
		mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
	}

	float idDelete;
	

	@Test
	public void getAllProducts_success() throws Exception {
			mockMvc.perform(MockMvcRequestBuilders.get("/products").contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk());
	}


	@Test
	@Order(value = 6)
	public void getProductById_failure() throws Exception {

		mockMvc.perform(MockMvcRequestBuilders.get("/products/4455464456sdfasdsgdrg5").contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isNotFound());
		}
	
	
	
	@Test
	@Order(value = 2)
	public void getProductByFilters_success() throws Exception {

		mockMvc.perform(get("/products/search?min_price=1&max_price=3100&q=macMini")).andExpect(status().isOk())
		.andExpect(content().contentType("application/json"));
	}


	@Test
	@Order(value = 1)
	public void createProduct_success() throws Exception {
		Product product = generateProdct(null, "macMini", "Roteador", 3000);

		Mockito.when(productRepository.save(product)).thenReturn(product);

		MockHttpServletRequestBuilder mockRequest = MockMvcRequestBuilders.post("/products")
				.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)
				.content(this.mapper.writeValueAsString(product));

		
		 mockMvc.perform(mockRequest)
		 .andExpect(status().isCreated());		 
		 
	}	

	
	public Product generateProdct(String id, String name, String description, float price) {
		Product product = new Product();
		product.setId(id);
		product.setDescription(description);
		product.setPrice(price);
		product.setName(name);
		return product;
		
	}
}